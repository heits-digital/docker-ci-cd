#!/bin/bash

docker build \
        -t heits/ci-cd-build:node-12.16.2-alpine3.11 \
        -f build-images/node/12.16.2/alpine3.11/Dockerfile \
    build-images/node/12.16.2/alpine3.11/

docker push heits/ci-cd-build:node-12.16.2-alpine3.11
