#!/bin/bash

docker build -t heits/ci-cd-deploy:lftp-alpine3.11 -f deploy-images/lftp/alpine3.11/Dockerfile deploy-images/lftp/alpine3.11/
docker push heits/ci-cd-deploy:lftp-alpine3.11
