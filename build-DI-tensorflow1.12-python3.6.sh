#!/bin/bash

docker build -t heits/ci-cd-deploy:tensorflow1.12-python3.6 -f deploy-images/tensorflow/1.x/python3.6/Dockerfile deploy-images/tensorflow/1.x/python3.6
docker push heits/ci-cd-deploy:tensorflow1.12-python3.6
